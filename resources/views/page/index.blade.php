@extends('layouts.app')

@section('content')

<div class="container">
<h1 class="text-center">Editar Seções Página</h1>



@if(Session::has('message'))
<div class="alert alert-success alert-dismissible fade show mt-2">
  {{ session('message') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif





@if($empty)

<a class="btn btn-primary" href="/page/create">Adicionar novo</a>


@endif
<table class="table table-striped mt-4">
  <thead>
    <tr>

      <th scope="col">Imagem de Fundo</th>
      <th scope="col">Texto de Destaque</th>
      <th scope="col">Nome do Jogo</th>
      <th scope="col">Frase</th>
      <th scope="col">Descrição do Formulário</th>

      <th>Actions</th>
    </tr>
  </thead>

  <tbody>

    @foreach($page as $section)
    <tr>
      <td>
        <img src="{{ asset("storage/images/" . $section->background_image) }}" alt="" srcset="" width="40">

      </td>
      <td>{{ $section->theme_text }}</td>
      <td>{{ $section->game_name }}</td>
      <td>{{ $section->phrase }}</td>
      <td>{{ $section->form_description }}</td>


     
      <td>

        <div class="d-flex">
          <a class="btn btn-info btn-sm mr-1" href="/page/{{ $section->id }}/edit">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
              <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
            </svg>
          </a>


          <form method="post" action="/page/{{ $section->id}}" onsubmit="return confirm('Are you sure?')">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger btn-sm">



              <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
              </svg>
            </button>
          </form>
        </div>
      </td>


    </tr>
    @endforeach


  </tbody>
</table>

</div>


@endsection