@extends('layouts.app')

@section('content')








<div class="container">

<h1 class="mb-2 text-center">Seções da Página</h1> <br>
<br>
<br>

<div class="row mb-2  mt-2 justify-content-center">
        <p>Imagem Atual: </p>

        <img width="70" class="mb-2" src="{{ asset("storage/images/" . $page->background_image)  }}" alt="">
       
    </div>
   <div class="row mb-2 justify-content-center">
   <a class="btn btn-primary" href="{{ asset("storage/images/" . $page->background_image)  }}">Ver Imagem no Tamanho Original</a>
   </div>


    <form action="/page/{{ $page->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
        <div class="row justify-content-center">

           <div class="col-lg-6">
           <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                </div>
                <div class="custom-file">
                    <input type="file"  class="custom-file-input" name="imagem" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="inputGroupFile01">Escolha uma foto</label>
                </div>
            </div>
           </div>
        </div>


        <div class="row justify-content-center">
            <div class="col-lg-6">
            <label for="texto_destaque">Texto de Destaque: </label>
            <input class="form-control" required type="text" name="texto_destaque" id="texto_destaque" value="{{$page->theme_text}}">
            </div>
        </div>

        <div class="row justify-content-center">
           <div class="col-lg-6">
           <label for="nome_jogo">Nome do Jogo: </label>
            <input class="form-control" required type="text" name="nome_jogo" id="nome_jogo" value="{{$page->game_name}}">
           </div>
        </div>
        <div class="row justify-content-center">
           <div class="col-lg-6">
           <label for="frase">Frase: </label>
            <input class="form-control" required type="text" name="frase" id="frase" value="{{$page->phrase}}">
           </div>
        </div>
        <div class="row justify-content-center">
           <div class="col-lg-6">
           <label for="descricao_form">Descricao do Formulário: </label>
            <input class="form-control" required type="text" name="descricao_form" id="descricao_form" value="{{$page->form_description}}">
           </div>
        </div>




       <div class="row justify-content-center">

        <input type="submit" value="Atualizar" class="btn btn-success btn-lg mt-2">
       </div>

    </form>



</div>


@endsection