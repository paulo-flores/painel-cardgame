@extends('layouts.app')

@section('content')








<div class="container">

    <h1 class="mb-4 text-center">Seções da Página</h1>



    <form action="/page" method="POST" class="mt-2" enctype="multipart/form-data">
        @csrf
        <div class="row justify-content-center mt-2">

            <div class="col-lg-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                    </div>
                    <div class="custom-file">
                        <input type="file" required class="custom-file-input" name="imagem" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                        <label class="custom-file-label" for="inputGroupFile01">Escolha uma foto</label>
                    </div>
                </div>
            </div>
        </div>


        <div class="row justify-content-center">
            <div class="col-lg-6">
                <label for="texto_destaque">Texto de Destaque: </label>
                <input class="form-control" required type="text" name="texto_destaque" id="texto_destaque">
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-6">
                <label for="nome_jogo">Nome do Jogo: </label>
                <input class="form-control" required type="text" name="nome_jogo" id="nome_jogo">
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <label for="frase">Frase: </label>
                <input class="form-control" required type="text" name="frase" id="frase">
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <label for="descricao_form">Descricao do Formulário: </label>
                <input class="form-control" required type="text" name="descricao_form" id="descricao_form">
            </div>

        </div>

        <div class="row justify-content-center">
        <input type="submit" value="Cadastrar" class="btn btn-success btn-lg mt-2">
        </div>







    </form>



</div>


@endsection