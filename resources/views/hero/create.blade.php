@extends('layouts.app')

@section('content')








<div class="container">

<h1 class="mb-2 text-center">Cadastro de Herói</h1> <br>
<br>
<br>



    <form action="/hero" method="POST" enctype="multipart/form-data">
    @csrf
        <div class="row justify-content-center">
            <div class="col-lg-6">

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                </div>
                <div class="custom-file">
                    <input type="file" required class="custom-file-input" name="imagem" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="inputGroupFile01">Escolha uma foto</label>
                </div>
            </div>
            </div>
        </div>


        <div class="row justify-content-center">
           <div class="col-lg-6">
           <label for="description">Texto de Descrição: </label>
            <input class="form-control" required type="text" name="description" id="description">
           </div>
        </div>


       
       <div class="row justify-content-center">
       <input type="submit" value="Cadastrar" class="btn btn-success btn-lg mt-2">
       </div>

    </form>



</div>


@endsection