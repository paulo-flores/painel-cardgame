@extends('layouts.app')

@section('content')








<div class="container">

<h1 class="mb-2  mt-2 text-center">Edite o Herói</h1> <br>
<br>
<br>



    <div class="row mb-2 mt-2 justify-content-center">
        <p>Imagem Atual: </p>

        <img width="70" src="{{ asset("storage/images/" . $hero->image)  }}" alt="">
    </div>
    <div class="row mb-2 justify-content-center">
   <a class="btn btn-primary" href="{{ asset("storage/images/" . $hero->image)  }}">Ver Imagem no Tamanho Original</a>
   </div>


    <form action="/hero/{{ $hero->id }}" method="POST" class="mt-2" enctype="multipart/form-data">
    @csrf
    @method('PUT')
        <div class="row justify-content-center">

        <div class="col-lg-6">
        <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                </div>
                <div class="custom-file">
                    <input type="file"  class="custom-file-input" name="imagem" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="inputGroupFile01">Escolha uma foto</label>
                </div>
            </div>
        </div>
        </div>


        <div class="row justify-content-center">

           <div class="col-lg-6">
           <label for="description">Texto de Descrição: </label>
            <input class="form-control" type="text" required name="description" id="description" value="{{ $hero->description }}" > 
           </div>
        </div>


     <div class="row justify-content-center">
           
        <input type="submit" value="Atualizar" class="btn btn-success btn-lg mt-2">
     </div>

    </form>



</div>


@endsection