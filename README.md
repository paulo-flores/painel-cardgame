## Painel Cardgame

#CRUD em Laravel e Bootstrap 4 para gerenciamento de conteúdo de uma Landing Page

# Para executar o projeto:

- Faça o download do projeto 
- Faça uma cópia do arquivo .env.example e renomeie para .env
- Execute o Comando "composer install"
- Execute o comando "php artisan key:generate" 
- Execute o comando "php artisan storage:link" 
- Crie um banco de dados e insira as informações de conexão no arquivo .env em seus respectivos campos.
- Execute o comando "php artisan migrate" para executar as migrations
- Execute o comando "php artisan serve" para executar o projeto através do servidor de desenvolvimento



