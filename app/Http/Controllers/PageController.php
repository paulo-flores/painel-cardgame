<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page  = Page::all();
        if ($page->count() < 1) {
            $empty = true;
            return view('page.index', compact('page', 'empty'));
        }

        $empty = false;
        return view('page.index', compact('page', 'empty'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $page  = Page::all();
        if($page->count() >= 1){
           
            $request->session()->flash(
                'error',
                "Já existem dados cadastrados nessa página!"
            );
            return redirect('/page');
        }   
        return view('page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([
            'texto_destaque' => 'required|string',
            'nome_jogo' => 'required|string',
            'frase' => 'required|string',
            'descricao_form' => 'required|string',
            'imagem' => 'required|image|mimes:jpeg,png,jpg|max:2048',

        ]);
        $imageName = time() . '.' . $request->imagem->getClientOriginalExtension();
        $request->file('imagem')->storeAs('public/images/', $imageName);
        Page::create([
            'theme_text' => $request->texto_destaque,
            'game_name' => $request->nome_jogo,
            'phrase' => $request->frase,
            'form_description' =>  $request->descricao_form,
            'background_image' => $imageName
        ]);
        $request->session()->flash(
            'message',
            "Dados cadastrados com sucesso!"
        );
        return redirect('/page');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::findOrFail($id);
        return view('page.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->hasFile('imagem')) {
            $request->validate([
                'texto_destaque' => 'required|string',
                'nome_jogo' => 'required|string',
                'frase' => 'required|string',
                'descricao_form' => 'required|string',
                'imagem' => 'required|image|mimes:jpeg,png,jpg|max:2048',

            ]);
            $page = Page::find($id);
            $imageName = time() . '.' . $request->imagem->getClientOriginalExtension();
            $request->file('imagem')->storeAs('public/images/', $imageName);
            $page->update([
                'theme_text' => $request->texto_destaque,
                'game_name' => $request->nome_jogo,
                'phrase' => $request->frase,
                'form_description' =>  $request->descricao_form,
                'background_image' => $imageName
            ]);
            $request->session()->flash(
                'message',
                "Página atualizada com sucesso!"
            );
            return redirect('/page');
        } else {
            Page::find($id)->update([
                'theme_text' => $request->texto_destaque,
                'game_name' => $request->nome_jogo,
                'phrase' => $request->frase,
                'form_description' =>  $request->descricao_form
            ]);
            $request->session()->flash(
                'message',
                "Página atualizada com sucesso!"
            );
            return redirect('/page');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (Page::findOrFail($id)->delete()) {
            $request->session()->flash(
                'message',
                "Página excluída com sucesso!"
            );
            return redirect('/page');
        }
    }
}
