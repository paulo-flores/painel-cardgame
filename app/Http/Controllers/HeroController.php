<?php

namespace App\Http\Controllers;

use App\Models\Hero;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HeroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $heroes = Hero::paginate(5);
        return view('hero.index', compact('heroes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hero.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required|string',
            'imagem' => 'required|image|mimes:jpeg,png,jpg|max:2048',

        ]);
        $imageName = time() . '.' . $request->imagem->getClientOriginalExtension();
        $request->file('imagem')->storeAs('public/images/', $imageName);
        //Storage::disk('local')->put('public/images/', $request->imagem);
        Hero::create([
            "image" => $imageName,
            "description" => $request->description
        ]);
        $request->session()->flash(
            'message',
            "Herói cadastrado com sucesso!"
        );
        return redirect('/hero');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hero = Hero::findOrFail($id);
        return view('hero.edit', compact('hero'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->hasFile('imagem')) {

            $request->validate([
                'description' => 'required|string',
                'imagem' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',

            ]);

            $hero = Hero::find($id);
            $imageName = time() . '.' . $request->imagem->getClientOriginalExtension();
            $request->file('imagem')->storeAs('public/images/', $imageName);
            $hero->update([
                'image' => $imageName,
                'description' => $request->description
            ]);

            return redirect('/hero');
            
        } else{
            $request->validate([
                'description' => 'required|string',
            ]);
            Hero::find($id)->update([ 'description' => $request->description]);
            $request->session()->flash(
                'message',
                "Herói atualizado com sucesso!"
            );
            return redirect('/hero');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if (Hero::findOrFail($id)->delete()) {
            $request->session()->flash(
                'message',
                "Herói excluído com sucesso!"
            );
            return redirect('/hero');
        }
    }
}
